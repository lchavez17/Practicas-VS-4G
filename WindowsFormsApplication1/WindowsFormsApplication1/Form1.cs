﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        uint cantidad;
        float precioUnitario;
        public Form1()
        {
            InitializeComponent();

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void ButtonOperacion_Click(object sender, EventArgs e)
        {
            UInt16 cantidad;
            float precioUnitario;
            float operacionPago;
            cantidad = Convert.ToUInt16(textBox4.Text);
            if (checkBox1.Checked == true)
            {
                precioUnitario = float.Parse(textBox5.Text, System.Globalization.NumberStyles.Float, new System.Globalization.CultureInfo("en-US")); //usamos el punto como decimal algo extraño estaba en español

                //precioUnitario = float.Parse(textBox5.Text);
                operacionPago = (float)cantidad * precioUnitario;
                if (cantidad > 100)
                {
                    lblName6.Text = "40%";
                    textBox1.Text = "" + operacionPago;
                    textBox2.Text = "" + (operacionPago * .40);
                    textBox3.Text = "" + (operacionPago * .60);
                }
                else if (cantidad >= 25 && cantidad <= 100)
                {
                    lblName6.Text = "20%";
                    textBox1.Text = "" + (float)operacionPago;
                    textBox2.Text = "" + (operacionPago * .20);
                    textBox3.Text = "" + (operacionPago * .80);
                }
                else if (cantidad >= 10 && cantidad <= 24)
                {
                    lblName6.Text = "10%";
                    textBox1.Text = "" + (float)operacionPago;
                    textBox2.Text = "" + (operacionPago * .10);
                    textBox3.Text = "" + (operacionPago * .90);
                }
                else if (cantidad < 10)
                {
                    lblName6.Text = "No aplica";

                    textBox1.Text = "no aplica";
                    textBox2.Text = "no aplica";
                    textBox3.Text = "" + operacionPago;
                }
            } else if (checkBox1.Checked == false)
            {

                precioUnitario = float.Parse(textBox5.Text);
                operacionPago = (float)cantidad * precioUnitario;
                if (cantidad > 100)
                {
                    lblName6.Text = "%40";
                    textBox1.Text = "" + operacionPago;
                    textBox2.Text = "" + (operacionPago * .40);
                    textBox3.Text = "" + (operacionPago * .60);
                }
                else if (cantidad >= 25 && cantidad <= 100)
                {
                    lblName6.Text = "20%";
                    textBox1.Text = "" + (float)operacionPago;
                    textBox2.Text = "" + (operacionPago * .20);
                    textBox3.Text = "" + (operacionPago * .80);
                }
                else if (cantidad >= 10 && cantidad <= 24)
                {
                    lblName6.Text = "10%";
                    textBox1.Text = "" + (float)operacionPago;
                    textBox2.Text = "" + (operacionPago * .10);
                    textBox3.Text = "" + (operacionPago * .90);
                }
                else if (cantidad < 10)
                {
                    lblName6.Text = "No aplica";
                    textBox1.Text = "no aplica";
                    textBox2.Text = "no aplica";
                    textBox3.Text = "" + operacionPago;
                }

            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void lblName6_Click(object sender, EventArgs e)
        {

        }
    }
}
