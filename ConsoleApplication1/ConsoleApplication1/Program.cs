﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaro variables Diferentes
            int numeroUno;
            short numeroChico;
            Console.Write("Dame un numero: "); //imprimo para que me de un numero
            numeroUno = int.Parse(Console.ReadLine()); //uso la funcion int.parse para convertir  el numero a entero.
            Console.Write("Dame Otro numero: ");
            numeroChico = Convert.ToInt16(Console.ReadLine()); //uso la funcion convert.toint16 para convertir el numero dos a entero.
            /* vamos hacer las funciones aritmeticas
             * para poder lograr nuestros objetivos
             * en este lenguaje tan comodo xD*/
            Console.Write("La suma de los numeros es: "+ (numeroUno + numeroChico));
            Console.Write("\nLa resta de los numeros es: " + (numeroUno - numeroChico));
            Console.Write("\nLa multplicacion de los numeros es: " + (numeroUno * numeroChico));
            Console.Write("\nLa divicion de los numeros es: " + Convert.ToDouble(numeroUno / numeroChico)); // por que no convierte a flotante??? :(
            Console.Read();


        }
    }
}
