﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication4
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1=72,num2=108, c=2, mcd = 1;
            
            while(c<=num1 && c <= num2)
            {
                while(num1%c==0 || num2%c==0)
                {
                    mcd = mcd * c;
                    num1 = num1 / c;
                    num2 = num2 / c;
                }
                c = c + 1;
            }
            Console.WriteLine("el numero es :" + mcd.ToString());
            Console.ReadLine();
        }
    }
}
