﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace mcdmcm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int num1 = 0, num2 = 0, mcd = 1, i = 2,mcm=0;

            num1 = Convert.ToInt32(textBox1.Text);
            num2 = Convert.ToInt32(textBox2.Text);

            while (i <= num1 && i <= num2)
            {
                while (num1 % i == 0 && num2 % i == 0)
                {
                    mcd = mcd * i;
                    num1 = num1 / i;
                    num2 = num2 / i;
                }
                i = i + 1;
            }
            textBox3.Text = "" + mcd.ToString();
            num1 = Convert.ToInt32(textBox1.Text);
            num2 = Convert.ToInt32(textBox2.Text);
            mcm = ((num1 * num2) / mcd);
            textBox4.Text = "" + mcm.ToString();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}