﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication4
{
    public partial class Form1 : Form
    {
        int[,] miMatriz;
        int tam;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            int cantRep, cant = 0, cont = 0, menor=0,mayor=0,suma=0 ;
            string cadena;
            tam = Convert.ToInt32(textBox1.Text);
            Random nAleatoreo = new Random();
            miMatriz = new int[tam,tam];
            for (int i = 0; i < tam; i++)
            {
                cadena = " | ";   
                    for (int j = 0; j < tam; j++)
                    {
                    miMatriz[i, j] = nAleatoreo.Next(1, 500);
                    cadena += miMatriz[i, j].ToString() + " | ";
                        if (i==j)
                        {
                            if (i==0 && j==0)
                            {
                                menor = miMatriz[i, j];
                                mayor = miMatriz[i, j];
                            }
                            if(menor> miMatriz[i,j])
                            {
                                menor = miMatriz[i, j];
                            }
                            if(mayor < miMatriz[i,j])
                            {
                                mayor = miMatriz[i, j];
                            }
                            suma += miMatriz[i, j];

                        }
                                        
                    }
                listBox1.Items.Add(cadena);
            }
            textBox2.Text = Convert.ToString(mayor);
            textBox3.Text = Convert.ToString(menor);
            textBox4.Text = (suma / (tam * tam)).ToString();
        }
    }
}
