﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cambioDeMoneda
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UInt32 pesos = 0;
            float  dolares = 20.4344361F, euros = 22.6901322F, libras= 25.3632221F;
            pesos = Convert.ToUInt32(txtName1.Text);

            txtName2.Text = "" + (float)(pesos / dolares);
            txtName3.Text = "" + (float)(pesos / euros);
            txtName4.Text = "" + (float)(pesos / libras);

        }
    }
}
