﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApplication3
{
    public partial class Form1 : Form
    {
        int[] miVector;
        int tam;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            listBox1.Items.Clear();
            int cantRep, cant = 0, cont = 0, moda=0 ;
            tam = Convert.ToInt32(textBox1.Text);
            Random nAleatoreo = new Random();
            miVector = new int[tam];
            for (int i = 0; i < miVector.Length; i++)
            {
                miVector[i]= nAleatoreo.Next(1, 500);
                listBox1.Items.Add(miVector[i]);
            }
            for (int i = 0; i <miVector.Length; i++)
            {
                cantRep = 0;
                for (int j = 0; j <miVector.Length; j++)
                {
                    if (miVector[i] == miVector[j])
                    {
                        cantRep++;
                    }
                    if (cantRep> cant)
                    {
                        cont++;
                        moda = miVector[i];
                        cant = cantRep;

                    }
                }

            }
            textBox2.Text = Convert.ToString(moda);
            textBox3.Text = Convert.ToString(cont);
        }
    }
}
