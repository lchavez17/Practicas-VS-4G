﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvsiaulPractica30
{
    public partial class Form2 : Form
    {
        private string nombre;
        public string nombrepass
        {
            get { return nombre; }
            set { nombre = value; }
        }

        private string clasifica;
        public string clasificapass
        {
            get { return clasifica; }
            set { clasifica = value; }
        }
        private string genero;
        public string generopass
        {
            get { return genero; }
            set { genero = value; }
        }

        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
           label6.Text= clasificapass;
           label4.Text = generopass;
            label2.Text = nombrepass;
            for (int i = 0; i < 10; i++)
            {
                listBox1.Items.Add("Pelicula " + (i+1));
            }
            for (int i = 0; i < 3; i++)
            {
                listBox1.Items.Add("....");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
