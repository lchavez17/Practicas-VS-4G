﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PvsiaulPractica30
{
    public partial class Form1 : Form
    {
        string nombre = "";    
        int genero = 0;
        int clasificacion = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.TextLength>0)
            {
                nombre += textBox1.Text;
                if (radioButton1.Checked) clasificacion = 1;
                if (radioButton2.Checked) clasificacion = 2;
                if (radioButton3.Checked) clasificacion = 3;
                if (radioButton4.Checked) genero = 4;
                if (radioButton5.Checked) genero = 5;
                if (radioButton6.Checked) genero = 6;
                Class1 clasifica = new Class1(clasificacion);
                Class2 generoC = new Class2(genero);
                Form2 forma2 = new Form2();
                forma2.clasificapass = clasifica.clasificaClaseResultado();
                forma2.generopass = generoC.generoClaseResultado();
                forma2.nombrepass = textBox1.Text ;
                forma2.Show();
                textBox1.Text = "";
                
            }
        }
    }
}
